/*
@Time : 2018/6/12 下午4:46
@Author : tengjufeng
@File : file
@Software: GoLand
*/

package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

/*

文件操作的大多数函数都是在os包里面，下面列举了几个目录操作的：

func Mkdir(name string, perm FileMode) error

创建名称为name的目录，权限设置是perm，例如0777

func MkdirAll(path string, perm FileMode) error

根据path创建多级子目录，例如astaxie/test1/test2。

func Remove(name string) error

删除名称为name的目录，当目录下有文件或者其他目录时会出错

func RemoveAll(path string) error

根据path删除多级子目录，如果path是单个名称，那么该目录下的子目录全部删除。


*/
/*
path: 路径
mod:  目录权限
all:  yes创建多级子目录，no只创建一个目录
*/
func makeDir(path string, mod os.FileMode, all string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
		} else {
			// other error
		}
	} else {
		//exist
		removDir(path, "yes")
	}
	var err error
	if all == "yes" {
		err = os.MkdirAll(path, mod)

	} else if all == "no" {
		err = os.Mkdir(path, mod)
	}
	if err != nil {
		fmt.Println("create path error ： ", err.Error())
		return false
	}
	return true
}
func removDir(path string, all string) bool {
	var err error
	switch all {
	case "yes":
		err = os.RemoveAll(path)
	case "no":
		err = os.Remove(path)
	}
	if err != nil {
		fmt.Println("remove path error : ", err.Error())
		return false
	}
	return true
}

func createFile(file string) bool {
	cf, err := os.Create(file)
	if err != nil {
		fmt.Println("create file error : ", err.Error())
		return false
	}
	defer cf.Close()

	cf.WriteString("abcdef")
	//打开文件
	f, _ := os.Open(file)
	defer f.Close()
	rd := bufio.NewReader(f)
	str, err := rd.ReadString('\n')
	if err == io.EOF {
		fmt.Println("read line end")
	} else if err != nil {
		return false
	}
	fmt.Println(str)
	return true
}
func main() {
	path := "./test"
	makeDir(path, 0777, "no")
	file := "./test/file.txt"
	createFile(file)
}
